//
//  AppDelegate.m
//  WebsiteRank
//
//  Created by macbook on 11/25/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "AppDelegate.h"
#import "CDDomain.h"
#import "CDNetwork.h"
#import "TestFlight.h"
#import "Flurry.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] identifierForVendor].UUIDString];
    [TestFlight setOptions:@{ TFOptionReportCrashes: @NO}];
    [TestFlight takeOff:@"134ec17a-f6dd-411e-a916-7f2de2a86b5c"];
    
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"8VTQDJHWV3XPV3532DZY"];
    
    if ([[CDAppSettings sharedSettings] valueForKey:@"CDAutoRefresh"]) {
        int ver = [[[UIDevice currentDevice] systemVersion] intValue];
        if (ver > 6) {
            switch ([[[CDAppSettings sharedSettings] valueForKey:@"CDAutoRefreshInterval"] intValue]) {
                case 1:
                    [application setMinimumBackgroundFetchInterval:60*60*12];
                    break;
                case 2:
                    [application setMinimumBackgroundFetchInterval:(60*60*12)*7];
                    break;
                case 3:
                    [application setMinimumBackgroundFetchInterval:(60*60*12)*14];
                    break;
                case 4:
                    [application setMinimumBackgroundFetchInterval:(60*60*12)*30];
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    [self _setUpCoreData];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    [TestFlight passCheckpoint:@"RUN_APP"];
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"footer-1"] forBarMetrics:UIBarMetricsDefault];
    
    return YES;
}

- (void)_setUpCoreData
{
    [MagicalRecord setupAutoMigratingStackWithSqliteStoreNamed:@"Model"];
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    BOOL isUpdateAviable = YES;
    if ([[CDAppSettings sharedSettings] valueForKey:@"CDWiFiLimitation"]) {
        isUpdateAviable = NO;
        if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWiFi) {
            isUpdateAviable = YES;
        }
    }
    
    if (isUpdateAviable) {
        NSFetchedResultsController *fetchedResultsController = [CDDomain MR_fetchAllSortedBy:nil ascending:YES withPredicate:nil groupBy:nil delegate:nil];
        NSArray *domainsArray = [fetchedResultsController fetchedObjects];
        [[[CDNetwork alloc] init] updateRanksForDomains:domainsArray completionHandler:^(){
            completionHandler(UIBackgroundFetchResultNewData);
        }];
    } else {
        completionHandler(UIBackgroundFetchResultNewData);
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
