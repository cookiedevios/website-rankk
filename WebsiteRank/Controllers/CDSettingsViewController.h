//
//  CDSettingsViewController.h
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDViewController.h"

typedef NS_ENUM(NSUInteger, CDIntervalPeriod){
    CDIntervalPeriodDay = 1,
    CDIntervalPeriodWeek = 2,
    CDIntervalPeriodTwoWeek = 3,
    CDIntervalPeriodMonth = 4,
};

@interface CDSettingsViewController : CDViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) CDIntervalPeriod selectedInterval;

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet UITableViewCell *everyDayCell;
@property (nonatomic) IBOutlet UITableViewCell *weekCell;
@property (nonatomic) IBOutlet UITableViewCell *twoWeekCell;
@property (nonatomic) IBOutlet UITableViewCell *monthCell;
@property (nonatomic) IBOutlet UISwitch *wiFiSwitch;
@property (nonatomic) IBOutlet UISwitch *autoupdateSwitch;

- (IBAction)autuUpdateStateDidChanged:(id)sender;
- (IBAction)wiFiStateDidChanged:(id)sender;
- (IBAction)aboutButtonDidClicked:(id)sender;
@end
