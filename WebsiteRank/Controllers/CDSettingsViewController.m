//
//  CDSettingsViewController.m
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDSettingsViewController.h"

@interface CDSettingsViewController ()

@end

@implementation CDSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[CDAppSettings sharedSettings] valueForKey:@"CDAutoRefresh"]) {
        	self.selectedInterval = [[[CDAppSettings sharedSettings] valueForKey:@"CDAutoRefreshInterval"] intValue];
        if (self.selectedInterval == 0) {
            self.selectedInterval = 1;
        }
    }
    if (self.selectedInterval != 0) {
        [self.autoupdateSwitch setOn: YES];
    }
}

#pragma mark tableView Delegete/Data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.row) {
        case 0:
            cell = self.everyDayCell;
            break;
        case 1:
            cell = self.weekCell;
            break;
        case 2:
            cell = self.twoWeekCell;
            break;
        case 3:
            cell = self.monthCell;
            break;
            
        default:
            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.backgroundColor = [UIColor clearColor];
    if (cell.tag != self.selectedInterval) {
        cell.accessoryView = nil;
        cell.textLabel.textColor = [UIColor darkGrayColor];
    } else {
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ok"]];
        cell.textLabel.textColor = [UIColor colorWithRed:0.25 green:0.7 blue:0.68 alpha:1.0f];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (IOS_VER > 6) {
        self.selectedInterval = indexPath.row + 1;
        switch (self.selectedInterval) {
            case CDIntervalPeriodDay:
                [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:60*60*12];
                break;
            case CDIntervalPeriodWeek:
                [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:(60*60*12)*7];
                break;
            case CDIntervalPeriodTwoWeek:
                [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:(60*60*12)*14];
                break;
            case CDIntervalPeriodMonth:
                [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:(60*60*12)*30];
                break;
                
            default:
                break;
        }
    }
    
    [self.autoupdateSwitch setOn: YES animated:YES];
    [[CDAppSettings sharedSettings] setValue:@"1" forKey:@"CDAutoRefresh"];
    [[CDAppSettings sharedSettings] setValue:@(indexPath.row + 1) forKey:@"CDAutoRefreshInterval"];
    self.selectedInterval = indexPath.row + 1;
    [self.tableView reloadData];
}

#pragma mark

- (IBAction)autuUpdateStateDidChanged:(UISwitch*)sender
{
    if (!sender.isOn) {
        [[CDAppSettings sharedSettings] removeValueForKey:@"CDAutoRefresh"];
        self.selectedInterval = 0;
        [self.tableView reloadData];
    } else {
        self.selectedInterval = [[[CDAppSettings sharedSettings] valueForKey:@"CDAutoRefreshInterval"] intValue];
        if (self.selectedInterval == 0) {
            self.selectedInterval = CDIntervalPeriodDay;
            [[CDAppSettings sharedSettings] setValue:@(1) forKey:@"CDAutoRefreshInterval"];
            [[CDAppSettings sharedSettings] setValue:@"1" forKey:@"CDAutoRefresh"];
        }
        [self.tableView reloadData];
    }
}

- (IBAction)wiFiStateDidChanged:(UISwitch*)sender
{
    if (!sender.isOn) {
        [[CDAppSettings sharedSettings] removeValueForKey:@"CDWiFiLimitation"];
        [self.tableView reloadData];
    } else {
        [[CDAppSettings sharedSettings] setValue:@"YES" forKey:@"CDWiFiLimitation"];
        [self.tableView reloadData];
    }
    
}

- (void)aboutButtonDidClicked:(id)sender
{
    
}

@end
