//
//  CDViewController.h
//  WebsiteRank
//
//  Created by macbook on 11/26/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ISiPad (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()?YES:NO)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IOS_VER [[[UIDevice currentDevice] systemVersion] intValue]

@interface CDViewController : UIViewController<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end
