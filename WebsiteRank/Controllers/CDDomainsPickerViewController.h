//
//  CDDomainsPickerViewController.h
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDViewController.h"

@class CDDomain;

@protocol CDDomainPickerDelegate <NSObject>

- (void)domainDidSelected:(CDDomain*)domain;

@end

@interface CDDomainsPickerViewController : CDViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,weak) id<CDDomainPickerDelegate>delegate;

@end
