//
//  SecondViewController.h
//  WebsiteRank
//
//  Created by macbook on 11/25/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDViewController.h"

@interface CDDomainsViewController : CDViewController<UITextFieldDelegate>

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet UITextField *domainNameTextField;
@property (nonatomic) IBOutlet UIView *bottomInputView;

- (IBAction)addButtonDidClicked:(id)sender;

@end
