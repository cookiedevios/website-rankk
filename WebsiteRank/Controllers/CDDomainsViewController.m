//
//  SecondViewController.m
//  WebsiteRank
//
//  Created by macbook on 11/25/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDDomainsViewController.h"
#import "CDEditDomainCell.h"

@interface CDDomainsViewController ()

@property (nonatomic) CGRect tmpRect;
@property (nonatomic) CGRect tmpTableViewRect;

@end

@implementation CDDomainsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!ISiPad) {
        [self performSelector:@selector(setupBottomBar) withObject:nil afterDelay:0.0];
    }
}

- (void)setupBottomBar
{
    float offset = -5;
    if (IOS_VER > 6) {
        offset = self.bottomInputView.frame.size.height + 20;
    }
    self.bottomInputView.frame = CGRectMake(self.bottomInputView.frame.origin.x, self.view.frame.size.height - self.tabBarController.tabBar.frame.size.height - offset, self.bottomInputView.frame.size.width, self.bottomInputView.frame.size.height);
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    self.tmpTableViewRect = self.tableView.frame;
    CGRect keyboardRect;
    [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardRect];
    float keyboardEndFrame = keyboardRect.origin.y;
    
    __block CGRect frame = self.bottomInputView.frame;
    self.tmpRect = frame;
    
    [UIView animateWithDuration:0.25 animations:^(){
        frame.origin.y = keyboardEndFrame - frame.size.height - 20;
        self.bottomInputView.frame = frame;
        self.tableView.frame = CGRectMake(self.tmpTableViewRect.origin.x, self.tmpTableViewRect.origin.y, self.tmpTableViewRect.size.width, frame.origin.y - self.tmpTableViewRect.origin.y);
    }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    CGRect keyboardRect;
    [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardRect];
    float keyboardEndFrame = keyboardRect.origin.y;
    
    [UIView animateWithDuration:0.25 animations:^(){
        CGRect frame = self.bottomInputView.frame;
        frame.origin.y = keyboardEndFrame - frame.size.height - 112;
        if (ISiPad || IS_IPHONE5) {
            frame.origin.y = frame.origin.y + 60;
        }
        self.bottomInputView.frame = self.tmpRect;
        self.tableView.frame = self.tmpTableViewRect;
    } completion:nil];
}

#pragma mark tableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CDEditDomainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    CDDomain *domain = self.fetchedResultsController.fetchedObjects[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.titleTextField.text = domain.domainString;
    cell.domain = self.fetchedResultsController.fetchedObjects[indexPath.row];
    
    return cell;
}

#pragma mark

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)addButtonDidClicked:(id)sender
{
    __weak id weakSelf = self;
    if ([self _isDomainNameValid]) {
        [self.domainNameTextField resignFirstResponder];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext* localContext){
            for (CDDomain *domain in self.fetchedResultsController.fetchedObjects) {
                if ([domain.domainString isEqualToString:[weakSelf domainNameTextField].text]) {
                    return ;
                }
            }
            CDDomain *d = [CDDomain MR_createInContext:localContext];
            d.domainString = [[weakSelf domainNameTextField].text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            dispatch_async(dispatch_get_main_queue(), ^(){
                [[weakSelf domainNameTextField] setText:@""];
            });
        }];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Incorrect domain name" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}

- (BOOL)_isDomainNameValid
{
    if (self.domainNameTextField.text.length == 0) {
        return NO;
    }
    if ([self.domainNameTextField.text rangeOfString:@"."].location == NSNotFound) {
        return NO;
    }
    return YES;
}

#pragma mark

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    dispatch_async(dispatch_get_main_queue(), ^(){
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    });
}


@end
