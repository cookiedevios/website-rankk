//
//  CDViewController.m
//  WebsiteRank
//
//  Created by macbook on 11/26/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDViewController.h"
#import "CDDomain.h"
#import "CDNetwork.h"

@interface CDViewController ()

@end

@implementation CDViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    self.fetchedResultsController = [CDDomain MR_fetchAllSortedBy:nil ascending:YES withPredicate:nil groupBy:nil delegate:self];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed: (UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()?@"fon":@"fon~ipad")]];
    imageView.tag = 999;
    imageView.center = CGPointMake(imageView.center.x, imageView.center.y + 20);
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    self.view.backgroundColor = [UIColor blackColor];
    
    UIImage * image = [self imageWithImage:[UIImage imageNamed:(UIUserInterfaceIdiomPad == UI_USER_INTERFACE_IDIOM()?@"footer-1":@"footer-2")] convertToSize:self.tabBarController.tabBar.frame.size];
    [self.tabBarController.tabBar setBackgroundImage:image];

    
    [super viewDidLoad];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
   
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{

}

@end
