//
//  CDDomainsPickerViewController.m
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDDomainsPickerViewController.h"

@interface CDDomainsPickerViewController ()

@end

@implementation CDDomainsPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor whiteColor];
    if (ISiPad) {
        [[self.view viewWithTag:999] removeFromSuperview];
    } else {
        UIView *bg = [self.view viewWithTag:999];
        bg.center = CGPointMake(bg.center.x, bg.center.y - 20);
    }
    [self performSelector:@selector(closeControlleWithoutData) withObject:nil afterDelay:1];
}

- (void)closeControlleWithoutData
{
    if (!ISiPad) {
        if (self.fetchedResultsController.fetchedObjects.count == 0) {
            if ([self.delegate respondsToSelector:@selector(domainDidSelected:)]) {
                [self.delegate domainDidSelected:nil];
            }
        }
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    CDDomain *domain = self.fetchedResultsController.fetchedObjects[indexPath.row];
    cell.textLabel.text = domain.domainString;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(domainDidSelected:)]) {
        [self.delegate domainDidSelected:self.fetchedResultsController.fetchedObjects[indexPath.row]];
    }
}


@end
