//
//  CDHistoryViewController.m
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDHistoryViewController.h"

@interface CDHistoryViewController ()

@end

@implementation CDHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
