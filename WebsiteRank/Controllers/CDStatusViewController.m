//
//  FirstViewController.m
//  WebsiteRank
//
//  Created by macbook on 11/25/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDStatusViewController.h"
#import "CDSiteRankCell.h"

@interface CDStatusViewController ()

@property (nonatomic) UIPopoverController *popover;
@property (nonatomic) CDDomainsPickerViewController *destController;
@property (nonatomic) CDDomain *selectedDomain;

@end

@implementation CDStatusViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IOS_VER > 6) {
        [self _setUpTabBar];
    }
}

- (void)_setUpTabBar
{
    UIImage *image = [UIImage imageNamed:@"StatusIcon"];
    UIImage *imageSel = [UIImage imageNamed:@"StatusIcon-high"];
    
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imageSel = [imageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Status" image:image selectedImage:imageSel];
    [self.tabBarController setSelectedIndex:0];
    
    image = [UIImage imageNamed:@"DomainsIcon"];
    imageSel = [UIImage imageNamed:@"DomainsIcon-high"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imageSel = [imageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.tabBarController.viewControllers[1] setTabBarItem:[[UITabBarItem alloc] initWithTitle:@"Domains" image:image selectedImage:imageSel]];
    
    image = [UIImage imageNamed:@"HistoryIcon"];
    imageSel = [UIImage imageNamed:@"HistoryIcon-high"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imageSel = [imageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.tabBarController.viewControllers[2] setTabBarItem:[[UITabBarItem alloc] initWithTitle:@"History" image:image selectedImage:imageSel]];
    
    image = [UIImage imageNamed:@"SettingsIcon"];
    imageSel = [UIImage imageNamed:@"SettingsIcon-high"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    imageSel = [imageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.tabBarController.viewControllers[3] setTabBarItem:[[UITabBarItem alloc] initWithTitle:@"Settings" image:image selectedImage:imageSel]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = @"competeCell";
    if (indexPath.row == 0) {
        cellId = @"alexaCell";
    }
    CDSiteRankCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    if (indexPath.row == 0) {
        cell.rankValueLabel.text = [self.selectedDomain getLatestAlexRank];
    } else if (indexPath.row == 1) {
        cell.rankValueLabel.text = [self.selectedDomain getLatestCompeteRank];
    }
    if (!self.selectedDomain) {
        cell.rankValueLabel.text = @"";
    } else {
        if (cell.rankValueLabel.text.length ==0) {
            cell.rankValueLabel.text = @"No Rating";
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

#pragma mark

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"domainSelection"]) {
        CDDomainsPickerViewController *controller = (CDDomainsPickerViewController *)segue.destinationViewController;
        controller.delegate = self;
        if (ISiPad) {
            self.popover = ((UIStoryboardPopoverSegue*)segue).popoverController;
        } else {
            self.destController = controller;
        }
    }
}


#pragma mark CDOmainPicjkerDelegate

- (void)domainDidSelected:(CDDomain *)domain
{
    if (ISiPad) {
        [self.popover dismissPopoverAnimated:YES];
        self.popover = nil;
    } else {
        [self.destController dismissViewControllerAnimated:(domain==nil?NO:YES) completion:nil];
    }
    if (!domain) {
        [[[UIAlertView alloc] initWithTitle:@"Empty list" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        return;
    }
    self.selectedDomain = nil;
    [self.tableView reloadData];
    self.selectedDomain = domain;
    [self.selectedDomainButton setTitle:domain.domainString forState:UIControlStateNormal];
    self.selectedDomainButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    CDNetwork * n = [CDNetwork new];
    [n getAlexaRankForDomain:domain completionHandler:nil];
    [n getCompeteRankForDomain:domain completionHandler:nil];
}

#pragma mark

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

@end
