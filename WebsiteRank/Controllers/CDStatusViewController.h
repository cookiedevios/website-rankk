//
//  FirstViewController.h
//  WebsiteRank
//
//  Created by macbook on 11/25/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDViewController.h"
#import "CDDomainsPickerViewController.h"

@interface CDStatusViewController : CDViewController<UITabBarDelegate,UITableViewDataSource,CDDomainPickerDelegate>

@property (nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) IBOutlet UIButton *selectedDomainButton;

@end
