//
//  CDSiteRankCell.h
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDSiteRankCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel *rankValueLabel;

@end
