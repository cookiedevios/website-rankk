//
//  CDEditDomainCell.h
//  WebsiteRank
//
//  Created by macbook on 11/28/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDEditDomainCell : UITableViewCell<UITextFieldDelegate,UIAlertViewDelegate>

@property (nonatomic) IBOutlet UIButton *editButton;
@property (nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic) IBOutlet UITextField *titleTextField;
@property (nonatomic) CDDomain *domain;

- (IBAction)editButtonDidClicked:(id)sender;
- (IBAction)deleteButtonDidClicked:(id)sender;

@end
