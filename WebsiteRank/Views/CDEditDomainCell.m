//
//  CDEditDomainCell.m
//  WebsiteRank
//
//  Created by macbook on 11/28/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDEditDomainCell.h"

@implementation CDEditDomainCell
{
    NSString *_tmpString;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _tmpString = @"";
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)editButtonDidClicked:(id)sender
{
    self.titleTextField.tag = 1;
    [self.titleTextField becomeFirstResponder];
}

- (void)deleteButtonDidClicked:(id)sender
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext* localContext){
        [self.domain.managedObjectContext deleteObject:self.domain];
    }];
}

- (BOOL)_isDomainNameValid
{
    if (self.titleTextField.text.length == 0) {
        return NO;
    }
    if ([self.titleTextField.text rangeOfString:@"."].location == NSNotFound) {
        return NO;
    }
    if ([self.titleTextField.text isEqualToString:_tmpString]) {
        return  NO;
    }
    return YES;
}


#pragma mark textFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if (self.titleTextField.tag == 0) {
        return NO;
    }
    _tmpString = textField.text;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.titleTextField.tag = 0;
    if ([self _isDomainNameValid]) {
        [[[UIAlertView alloc] initWithTitle:@"Внимание!" message:@"Удалить историю прошлого домена?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
    } else {
        self.titleTextField.text = _tmpString;
    }
}
#pragma mark

#pragma mark AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
//            [MagicalRecord saveWithBlock:^(NSManagedObjectContext* localContext){
//                self.domain.domainString = self.titleTextField.text;
//            }];
            self.titleTextField.text = _tmpString;
        }
            break;
        case 1:
        {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext* localContext){
                self.domain.domainString =  self.titleTextField.text;
                self.domain.alexaRank = nil;
                self.domain.competeRank = nil;
            }];
        }
            break;
            
        default:
            break;
    }
}

@end
