//
//  ASSettings.h
//  ASCoreServices
//
//  Created by Vitaly Evtushenko on 08.05.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CDAppSettings : NSObject {
    @private
    NSMutableDictionary *data;
    NSString *path;
}

+ (CDAppSettings *)sharedSettings;

- (void)removeValueForKey:(NSString *)key;

- (id)valueForKey:(NSString *)key;

- (void)setValue:(id)value forKey:(NSString *)key;

@end
