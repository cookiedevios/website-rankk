//
//  CDAlexaRankPArser.h
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDAlexaRankPArser : NSObject <NSXMLParserDelegate>

@property (nonatomic) NSMutableDictionary *rank;

@end
