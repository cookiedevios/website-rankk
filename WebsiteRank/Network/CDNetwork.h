//
//  CDNetwork.h
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDDomain.h"

typedef void(^CDCompletionHandler)();

@interface CDNetwork : NSObject

- (void)getAlexaRankForDomain:(CDDomain*)domain completionHandler:(CDCompletionHandler)handler;;
- (void)getCompeteRankForDomain:(CDDomain*)domain completionHandler:(CDCompletionHandler)handler;;

- (void)updateRanksForDomains:(NSArray*)domainsArray completionHandler:(CDCompletionHandler)handler;

@end
