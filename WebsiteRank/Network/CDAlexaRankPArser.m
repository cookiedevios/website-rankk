//
//  CDAlexaRankPArser.m
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDAlexaRankPArser.h"

@implementation CDAlexaRankPArser

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    self.rank = [NSMutableDictionary new];
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([@"POPULARITY" isEqualToString:elementName]) {
        [self.rank setValue:attributeDict[@"TEXT"] forKey:@"currentValue"];
    }
    if([@"REACH" isEqualToString:elementName]) {
        [self.rank setValue:attributeDict[@"RANK"] forKey:@"previousRank"];
    }
    if([@"RANK" isEqualToString:elementName]) {
        [self.rank setValue:attributeDict[@"DELTA"] forKey:@"DELTA"];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName;
{
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
}


@end
