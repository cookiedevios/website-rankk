//
//  CDNetwork.m
//  WebsiteRank
//
//  Created by macbook on 11/27/13.
//  Copyright (c) 2013 CookieDev. All rights reserved.
//

#import "CDNetwork.h"
#import "CDAlexaRankPArser.h"
#import "CDDomain.h"

static NSString *competeApiKey = @"153340620258b5709ccd4c12a51420a9";

@implementation CDNetwork

- (void)getAlexaRankForDomain:(CDDomain*)domain completionHandler:(CDCompletionHandler)handler
{
    NSString *url = [NSString stringWithFormat:@"http://data.alexa.com/data?cli=10&url=%@",domain.domainString];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFXMLParserResponseSerializer serializer];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *requestOperation, NSXMLParser *xmlParser) {
        CDAlexaRankPArser *parser = [CDAlexaRankPArser new];
        xmlParser.delegate = parser;
        [xmlParser parse];
        
        if ([CDDomain isAlexaRankVerified:parser.rank]) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                [domain updateAlexaRankFromDictionary:parser.rank];
                if (handler) {
                    handler();
                }
            }];
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (handler) {
            handler();
        }
        
    }];
    [operation start];
}

- (void)getCompeteRankForDomain:(CDDomain *)domain completionHandler:(CDCompletionHandler)handler
{
    NSString *url = [NSString stringWithFormat:@"https://apps.compete.com/sites/%@/trended/rank/?apikey=%@",domain.domainString,competeApiKey];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *requestOperation, NSDictionary *jSON) {
        if ([CDDomain isCompeteRankVerified:jSON]) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                [domain updateCompeteRankFromDictionary:jSON];
                if (handler) {
                    handler();
                }
            }];
        }
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (handler) {
            handler();
        }
    }];
    
    [operation start];
}

- (void)updateRanksForDomains:(NSArray *)domainsArray completionHandler:(CDCompletionHandler)handler
{
    __block int competeUpdatesCount = 0;
    __block int alexaUpdatesCount = 0;
    for (CDDomain *domain in domainsArray) {
        [self getCompeteRankForDomain:domain completionHandler:^(){
            competeUpdatesCount++;
            if (alexaUpdatesCount == domainsArray.count && competeUpdatesCount == domainsArray.count) {
                handler();
            }
        }];
        [self getAlexaRankForDomain:domain completionHandler:^(){
            alexaUpdatesCount++;
            if (alexaUpdatesCount == domainsArray.count && competeUpdatesCount == domainsArray.count) {
                handler();
            }
        }];
    }
}


@end
