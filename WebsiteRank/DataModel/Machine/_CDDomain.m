// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CDDomain.m instead.

#import "_CDDomain.h"

const struct CDDomainAttributes CDDomainAttributes = {
	.alexaRank = @"alexaRank",
	.competeRank = @"competeRank",
	.domainString = @"domainString",
};

const struct CDDomainRelationships CDDomainRelationships = {
};

const struct CDDomainFetchedProperties CDDomainFetchedProperties = {
};

@implementation CDDomainID
@end

@implementation _CDDomain

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Domain" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Domain";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Domain" inManagedObjectContext:moc_];
}

- (CDDomainID*)objectID {
	return (CDDomainID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic alexaRank;






@dynamic competeRank;






@dynamic domainString;











@end
