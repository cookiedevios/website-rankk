// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CDDomain.h instead.

#import <CoreData/CoreData.h>


extern const struct CDDomainAttributes {
	__unsafe_unretained NSString *alexaRank;
	__unsafe_unretained NSString *competeRank;
	__unsafe_unretained NSString *domainString;
} CDDomainAttributes;

extern const struct CDDomainRelationships {
} CDDomainRelationships;

extern const struct CDDomainFetchedProperties {
} CDDomainFetchedProperties;






@interface CDDomainID : NSManagedObjectID {}
@end

@interface _CDDomain : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (CDDomainID*)objectID;





@property (nonatomic, strong) NSData* alexaRank;



//- (BOOL)validateAlexaRank:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSData* competeRank;



//- (BOOL)validateCompeteRank:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* domainString;



//- (BOOL)validateDomainString:(id*)value_ error:(NSError**)error_;






@end

@interface _CDDomain (CoreDataGeneratedAccessors)

@end

@interface _CDDomain (CoreDataGeneratedPrimitiveAccessors)


- (NSData*)primitiveAlexaRank;
- (void)setPrimitiveAlexaRank:(NSData*)value;




- (NSData*)primitiveCompeteRank;
- (void)setPrimitiveCompeteRank:(NSData*)value;




- (NSString*)primitiveDomainString;
- (void)setPrimitiveDomainString:(NSString*)value;




@end
