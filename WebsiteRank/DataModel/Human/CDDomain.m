#import "CDDomain.h"


@interface CDDomain ()

// Private interface goes here.

@end


@implementation CDDomain

#pragma mark Alexa

+ (BOOL)isAlexaRankVerified:(NSDictionary *)dict
{
    if (!dict || dict.count == 0) {
        return NO;
    }
    return YES;
}

- (void)updateAlexaRankFromDictionary:(NSDictionary *)dict
{
    NSData *rawData = [self alexaRank];
    NSMutableDictionary *rankDictionary = (NSMutableDictionary*)[NSKeyedUnarchiver unarchiveObjectWithData:rawData];
    if (!rankDictionary) {
        rankDictionary = [NSMutableDictionary new];
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYY MM dd";
    NSString *key = [formatter stringFromDate:[NSDate date]];
    if (![[rankDictionary allKeys] containsObject:key]) {
        [rankDictionary setValue:dict forKey: key];
        NSData *new = [NSKeyedArchiver archivedDataWithRootObject:rankDictionary];
        [self setAlexaRank:new];
    }
}

- (NSDictionary *)alexaRankDictionary
{
    return (NSMutableDictionary*)[NSKeyedUnarchiver unarchiveObjectWithData:[self alexaRank]];
}

- (NSString *)getLatestAlexRank
{
    NSDictionary *dict = [self alexaRankDictionary];
    NSArray *keys = [dict allKeys];
    if (dict && dict.count > 0) {
        return dict[keys.lastObject][@"currentValue"];
    }
    return nil;
}

#pragma mark
#pragma markCompete

+ (BOOL)isCompeteRankVerified:(NSDictionary *)dict
{
    if (dict && dict.count > 0) {
        NSArray *ranks = dict[@"data"][@"trends"][@"rank"];
        if (ranks && ranks.count > 0) {
            return YES;
        }
    }
    return NO;
}

- (void)updateCompeteRankFromDictionary:(NSDictionary *)dict
{
    NSData *rawData = [self competeRank];
    NSMutableArray *ranks = (NSMutableArray*)[NSKeyedUnarchiver unarchiveObjectWithData:rawData];
    if (!ranks) {
        ranks = [NSMutableArray new];
    }
    NSArray *rawRanks = dict[@"data"][@"trends"][@"rank"];
    for (NSDictionary * rank in rawRanks) {
        BOOL duplicated = NO;
        for (NSDictionary * oldrank in ranks) {
            if ([oldrank[@"date"] isEqualToString:rank[@"date"]]) {
                duplicated = YES;
                break;
            }
        }
        if (!duplicated) {
            [ranks insertObject:rank atIndex:0];
        }
    }
    NSData *new = [NSKeyedArchiver archivedDataWithRootObject:ranks];
    [self setCompeteRank:new];

}

- (NSArray *)competeRankDictionary
{
    return (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithData:[self competeRank]];
}

- (NSString *)getLatestCompeteRank
{
    NSArray *arr = [self competeRankDictionary];
    if (arr.count > 0) {
        NSString *value = [NSString stringWithFormat:@"%@", arr[0][@"value"]];
        return value;
    }
    return @"";
}

- (NSString *)description
{
    return self.domainString;
}

@end
