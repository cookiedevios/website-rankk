#import "_CDDomain.h"

@interface CDDomain : _CDDomain {}

+ (BOOL)isAlexaRankVerified:(NSDictionary*)dict;
    
- (void)updateAlexaRankFromDictionary:(NSDictionary*)dict;

- (NSDictionary*)alexaRankDictionary;

- (NSString*)getLatestAlexRank;

+ (BOOL)isCompeteRankVerified:(NSDictionary*)dict;

- (void)updateCompeteRankFromDictionary:(NSDictionary*)dict;

- (NSArray*)competeRankDictionary;

- (NSString*)getLatestCompeteRank;

@end
